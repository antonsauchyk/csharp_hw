﻿namespace DummyDemo.Homework
{
    public static class Task_6
    {
        public static void GetNumbersDivByBirthDay()
        {
            Console.WriteLine("Task 6: find numbers divisible by my birth day");

            Console.WriteLine("Please enter your birth day:");
            int userBirthDay = Int16.Parse(Console.ReadLine());
            Helper.ConsoleWriteLines(1);

            // Generate array in a range from 100 to 800
            int[] numbers = Enumerable.Range(100, 800).ToArray();

            // Filter array elements
            int[] resultNumbers = numbers.Where(number => number % userBirthDay == 0).ToArray();

            // Concatenate filtered elements and write them in console
            Console.WriteLine($"Result: {string.Join(" ", resultNumbers)}");
            Helper.ConsoleWriteLines(2);
        }
    }
}
