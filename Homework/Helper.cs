﻿namespace DummyDemo.Homework
{
    public static class Helper
    {
        public static void ConsoleWriteLines(int number)
        {
            for (int i = 0; i < number; i++)
            {
                Console.WriteLine();
            }
        }
    }
}
