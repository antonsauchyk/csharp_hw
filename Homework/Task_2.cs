﻿namespace DummyDemo.Homework
{
    public static class Task_2
    {
        public static void WriteNumbersDivBy5()
        {
            Console.WriteLine("Task 2: write two-digit numbers divisible by 5");
            Helper.ConsoleWriteLines(1);

            for (int i = 10; i < 100; i++)
            {
                if (i % 5 == 0)
                {
                    Console.Write($"{i} ");
                }
            }

            Helper.ConsoleWriteLines(2);
        }
    }
}
