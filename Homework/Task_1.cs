﻿namespace DummyDemo.Homework
{
    public static class Task_1
    {
        public static void WalkThroughArrayElements()
        {
            Console.WriteLine("Task 1: write all numbers of a hardcoded array");
            Helper.ConsoleWriteLines(1);

            int[] numbersArray = { 1, 2, 3, 4, 5, 6, 23, 32 };
            
            foreach (int number in numbersArray)
            {
                Console.WriteLine(number);
            }

            Helper.ConsoleWriteLines(2);
        }
    }
}
