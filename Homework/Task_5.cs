﻿namespace DummyDemo.Homework
{
    public static class Task_5
    {
        public static void AnalyzeUserNumbers()
        {
            Console.WriteLine("Task 5: analyze N user numbers");

            Console.WriteLine("Please enter the number of values you want to analyze:");
            int userInputNumberOfValues = Int16.Parse(Console.ReadLine());
            Helper.ConsoleWriteLines(1);

            int[] userNumbers = new int[userInputNumberOfValues];

            for (int i = 0; i < userInputNumberOfValues; i++)
            {
                Console.WriteLine($"Enter the number {i+1}:");
                userNumbers[i] = Int16.Parse(Console.ReadLine());       
            }

            int numberNegatives = userNumbers.Count(x => x < 0);
            int numberPositives = userNumbers.Count(x => x > 0);
            int numberZeros = userNumbers.Count(x => x == 0);

            Console.WriteLine($"The number of negative numbers: {numberNegatives}");
            Console.WriteLine($"The number of positive numbers: {numberPositives}");
            Console.WriteLine($"The number of zero values: {numberZeros}");
            Helper.ConsoleWriteLines(2);
        }
    }
}
