﻿namespace DummyDemo.Homework
{
    public static class Task_4
    {
        public static void MoveWASD()
        {
            Console.WriteLine("Task 4: move WASD");
            Console.WriteLine("Please enter W, A, S, or D");
            ConsoleKeyInfo userInput = Console.ReadKey();
            Helper.ConsoleWriteLines(1);

            string pressedKey = userInput.Key switch
            {
                ConsoleKey.W => "You moved up",
                ConsoleKey.A => "You moved to the left",
                ConsoleKey.S => "You moved down",
                ConsoleKey.D => "You moved to the right",
                _ => "Unknown command: you didn't move anywhere"
            };

            Console.WriteLine(pressedKey);
            Helper.ConsoleWriteLines(2);
        }
    }
}
