﻿namespace DummyDemo.Homework
{
    public static class Task_3
    {
        public static void WriteEachNumberFromNewLine()
        {
            Console.WriteLine("Task 3: write each number from a new line (-20, -40 ...)");
            Helper.ConsoleWriteLines(1);

            int i = 0;

            do
            {
                i -= 20;
                Console.WriteLine(i);

            } while (i > -1000);

            Helper.ConsoleWriteLines(2);
        }
    }
}
