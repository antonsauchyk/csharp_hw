﻿using System.Text.RegularExpressions;

namespace DummyDemo.Homework
{
    public static class Task_7
    {
        public static void GetEverySecondLetter()
        {
            const string verseText = @"Doesn't seem to matter what I do
                                    I'm always number two
                                    No one knows how hard I tried, oh-oh
                                    I, I have feelings that I can't explain
                                    Drivin' me insane
                                    All my life, been so polite
                                    But I'll sleep alone tonight
                                    'Cause I'm just Ken, anywhere else I'd be a ten
                                    Is it my destiny to live and die a life of blonde fragility?
                                    I'm just Ken
                                    Where I see love, she sees a friend
                                    What will it take for her to see the man behind the tan and fight for me?
                                    I wanna know what it's like to love, to be the real thing
                                    Is it a crime? Am I not hot when I'm in my feelings?
                                    And is my moment finally here, or am I dreaming?
                                    I'm no dreamer
                                    Can you feel the Kenergy?
                                    Feels so real, my Kenergy
                                    Can you feel the Kenergy?
                                    Feels so real, my Kenergy";

            Console.WriteLine("Task 7: get every second letter from a verse");

            // Trim and clean the verse from non-alphanumeric symbols
            Regex regexPattern = new Regex(@"[^a-zA-z\d\s:]");
            string verseTextCleaned = regexPattern.Replace(verseText.Trim(), "");

            // Split the verse by space ignoring empty values
            string[] verseWords = verseTextCleaned.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            // Generate array of second letters of each word. If a word is shorter - return the first letter.
            string[] everySecondLetter = new string[verseWords.Length];

            for (int i = 0; i < verseWords.Length; i++)
            {
                everySecondLetter[i] = verseWords[i].Length > 1 ? verseWords[i][1].ToString() : verseWords[i][0].ToString();
            }

            Console.WriteLine($"Result: {string.Join(" ", everySecondLetter)}");
            Helper.ConsoleWriteLines(2);
        }
    }
}
