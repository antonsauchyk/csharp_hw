﻿using System.Text;

namespace DummyDemo
{
    public static class Practice
    {
        private static string _privateField = "This field is for demonstration purposes only";

        public static bool ExampleMethod()
        {
            return true;
        }

    }
}
